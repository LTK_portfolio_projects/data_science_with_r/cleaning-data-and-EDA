# Exploratory Data Analysis of Global Temperature Data

This document outlines the steps taken in the Exploratory Data Analysis (EDA) of a global temperature dataset using R. The analysis involves data loading, cleaning, visualization, and statistical examination to uncover insights about global temperature trends.

## Overview

This document provides a high-level overview of the Exploratory Data Analysis (EDA) conducted on the global temperature dataset. Detailed methodologies and findings are elaborated upon in the accompanying `.Rmd` file which is published on to my [RPub Portfolio](https://rpubs.com/maxlamcoco/portfolio-EDA) for public access.

## Dataset Information

The dataset is retrieved from [Kaggle](https://www.kaggle.com/datasets/berkeleyearth/climate-change-earth-surface-temperature-data).

### Introduction to the Climate Change Dataset

This dataset, sourced from the Berkeley Earth Surface Temperature Study and affiliated with the Lawrence Berkeley National Laboratory, provides a comprehensive view of global temperature trends. It encompasses 1.6 billion temperature reports from 16 distinct archives, showcasing the evolution of temperature measurement from early mercury thermometers to modern electronic devices. The dataset has undergone extensive cleaning and preparation, making it a pivotal resource for understanding both historical and contemporary climate patterns.

The primary file, `GlobalTemperatures.csv`, tracks global average temperatures from 1750 for land and 1850 for combined land and ocean readings. It includes data on average, maximum, and minimum temperatures, along with their 95% confidence intervals. Additional files offer temperature trends by country, state, major city, and city, providing insights into localized climate behaviors.

For this project, the focus will be on the `GlobalTemperatures.csv` file from the dataset, conducting EDA to gain insights into general global temperature trends.

## Key Stages of Analysis

### Data Cleaning

-   Refinement of the dataset for accuracy and usability.
-   Activities included renaming columns, adjusting data types, and handling missing values.

### Data Visualization

-   Creating visual representations to identify patterns, trends, and anomalies in the data.
-   Utilized various plotting techniques for a comprehensive understanding of temperature trends.

### Outlier Detection

-   Identification and examination of outliers within the data.
-   Ensured the reliability and validity of the analysis.

### Conclusion and Further Analysis Suggestions

-   Synthesis of findings and insights gained from the EDA.
-   Proposals for potential future explorations based on the analysis.

For more detailed information, please refer to the full `.Rmd` file.
